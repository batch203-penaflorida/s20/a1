let setNumber = parseInt(prompt("Give me a number:"));
console.log(`The number you provided is ${setNumber}`);
for (let i = 0; i < setNumber; i++) {
  if (setNumber <= 50) {
    console.log(`The current value is at 50. Terminating the loop.`);
    break;
  }
  if (i % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number");
    continue;
  }
  if (i % 5 === 0) {
    setNumber -= 5;
    console.log(setNumber);
    setNumber -= 5;
    continue;
  }
}

let longestWord = "supercalifragilisticexpialidocious";
let consonants = "";

for (let i = 0; i < longestWord.length; i++) {
  if (
    longestWord[i] === "a" ||
    longestWord[i] === "e" ||
    longestWord[i] === "i" ||
    longestWord[i] === "o" ||
    longestWord[i] === "u"
  ) {
    continue;
  } else {
    consonants += longestWord[i];
  }
}
console.log(longestWord);
console.log(consonants);
